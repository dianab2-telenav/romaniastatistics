package file_processing;

import file_processing.models.Node;
import file_processing.models.Way;
import file_processing.udf.TagValue;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.io.Serializable;
import java.util.List;

import static org.apache.spark.sql.functions.*;


public class DatasetFilter implements Serializable {

    public Dataset<Row> selectLights(final Dataset<Row> relationsSet) {
        final Dataset<Row> lightsData = relationsSet.filter(array_contains(col("tags.key"), "highway")
                .and(array_contains(col("tags.value"), "street_lamp")));
        return lightsData;
    }

    public Dataset<Way> selectWay(SparkSession sparkSession, final Dataset<Way> waysSet, final String wayType) {
        final Dataset<Way> filtered = waysSet.filter(callUDF("isWayOfType", col("tags"), lit(wayType)));
        return filtered;
    }

    public Dataset<Way> selectCity(final Dataset<Way> waysSet, final Integer level, final String cityName) {
        final Dataset<Way> filtered = waysSet.filter(callUDF("isCity", col("tags"), lit(level), lit(cityName)));
        return filtered;
    }

    public Dataset<Way> selectFromWays(final String udfName, final TagValue tagValue, final Dataset<Way> waysSet) {
        final Dataset<Way> filtered =
                waysSet.filter(callUDF(udfName, col("tags"), lit(tagValue.getKey().toUpperCase())));
        return filtered;
    }

    public Dataset<Node> selectFromNodes(final String udfName, final TagValue tagValue, final Dataset<Node> nodesSet) {
        final Dataset<Node> filtered =
                nodesSet.filter(callUDF(udfName, col("tags"), lit(tagValue.getKey().toUpperCase())));
        return filtered;
    }
}
