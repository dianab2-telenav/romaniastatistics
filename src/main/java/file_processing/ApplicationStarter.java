package file_processing;

import file_processing.udf.*;
import file_processing.models.IndexedNode;
import file_processing.models.Node;
import file_processing.models.Way;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import scala.Tuple2;

import java.io.Serializable;
import java.util.List;


public class ApplicationStarter implements Serializable {

    private final String NODE_FILE_PATH = "./src/main/resources/romania-latest.osm.pbf.node.parquet";
    private final String RELATION_FILE_PATH = "./src/main/resources/romania-latest.osm.pbf.relation.parquet";
    private final String WAY_FILE_PATH = "./src/main/resources/romania-latest.osm.pbf.way.parquet";
    private final String ENTITY_TYPE_UDF = "isEntityOfType";
    private final String IS_WAY_UDF = "isWayOfType";
    private final String IS_CITY_UDF = "isCity";
    private final Dataset<Way> waysSet;
    private final Dataset<Node> nodesSet;

    public ApplicationStarter(SparkSession spark) {
        final ParquetReader parquetReader = new ParquetReader();
        spark.udf().register(ENTITY_TYPE_UDF, new TagsDetectorUDF(), DataTypes.BooleanType);
        waysSet = parquetReader.readWays(WAY_FILE_PATH, spark);
        nodesSet = parquetReader.readNodes(NODE_FILE_PATH, spark);

    }

    private long compute(String UDFname, TagValue tagValue) {
        final DatasetFilter datasetFilter = new DatasetFilter();
        final Dataset<Way> ways = datasetFilter.selectFromWays(UDFname, tagValue, waysSet);
        final Dataset<Node> nodes = datasetFilter.selectFromNodes(UDFname, tagValue, nodesSet);
        return nodes.count() + ways.count();
    }

    public void startChurchCounter(SparkSession spark) {
        final long total = compute(ENTITY_TYPE_UDF, TagValue.CHURCH);
        System.out.println("Number of churches mapped " + total);
    }

    public void startSchoolCounter(SparkSession spark) {
        final long total = compute(ENTITY_TYPE_UDF, TagValue.EDUCATIONAL);
        System.out.println("Number of schools mapped " + total);
    }

    public void startMedicalCounter(SparkSession spark) {
        final long total = compute(ENTITY_TYPE_UDF, TagValue.MEDICAL);
        System.out.println("Number of medical institutions mapped " + total);
    }

    public JavaPairRDD<Long, IndexedNode> getHighWayOfTypeList(SparkSession spark, String typeOfWay,
            Function<Tuple2<Long, IndexedNode>, Boolean> filter) {
        spark.udf().register(IS_WAY_UDF, new WayTypeDetectorUDF(), DataTypes.BooleanType);
        final DataJoiner dataJoiner = new DataJoiner();
        final DatasetFilter datasetFilter = new DatasetFilter();
        final Dataset<Way> motorways = datasetFilter.selectWay(spark, waysSet, typeOfWay);
        final JavaPairRDD<Long, IndexedNode> joined = dataJoiner.join(motorways.javaRDD(), nodesSet.toJavaRDD()).filter(filter);
        return joined;
    }

    public List<Tuple2<String, Double>> getBoundsOfAnAdministrative(SparkSession spark, String cityName,
            Integer adminLevel) {
        spark.udf().register(IS_CITY_UDF, new CitiesDetectorUDF(), DataTypes.BooleanType);
        final DataJoiner dataJoiner = new DataJoiner();
        final BoundaryUtil boundaryUtil = new BoundaryUtil();
        final DatasetFilter datasetFilter = new DatasetFilter();
        final Dataset<Way> cityWays = datasetFilter.selectCity(waysSet, adminLevel, cityName);
        final JavaPairRDD<Long, IndexedNode> joined = dataJoiner.join(cityWays.javaRDD(), nodesSet.toJavaRDD());
        return boundaryUtil.computeBounds(dataJoiner.mapToNodeList(joined));
    }

    public void startMotorwayComputer(SparkSession spark) { //test method
        final DistanceComputer distanceComputer = new DistanceComputer();
        final JavaPairRDD<Long, IndexedNode> joined =
                getHighWayOfTypeList(spark, TagValue.HIGHWAY.getValues().get(0), e -> true);
        System.out.println(distanceComputer.compute(joined));
    }

    public void startCityCounter(SparkSession spark) { //test method
        BoundaryUtil boundaryUtil = new BoundaryUtil();
        List<Tuple2<String, Double>> bounds = getBoundsOfAnAdministrative(spark, "Cluj", 4);
        for (Tuple2 tuple2 : bounds) {
            System.out.println(tuple2._1() + "   " + tuple2._2());
        }
        final DistanceComputer distanceComputer = new DistanceComputer();
        final JavaPairRDD<Long, IndexedNode> joinedWays =
                getHighWayOfTypeList(spark, TagValue.HIGHWAY.getValues().get(0), e -> boundaryUtil.isInside(bounds, e._2().getNode()));
        System.out.println(distanceComputer.compute(joinedWays));
    }

}
