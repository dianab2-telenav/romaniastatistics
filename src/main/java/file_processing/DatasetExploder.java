package file_processing;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.io.Serializable;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.explode;


public class DatasetExploder implements Serializable {

    public Dataset<Row> explodeRelations(final Dataset<Row> relationsSet) {
        relationsSet.cache();
        final Dataset<Row> explodedRelations1 = relationsSet.withColumn("tag", explode(col("tags")));
        final Dataset<Row> explodedRelations2 = explodedRelations1.withColumn("member", explode(col("members")));
        relationsSet.unpersist();
        return explodedRelations2;
    }

    public Dataset<Row> explodeNodes(final Dataset<Row> nodesSet) {
        nodesSet.cache();
        final Dataset<Row> explodedNodes = nodesSet.withColumn("tag", explode(col("tags")));
        nodesSet.unpersist();
        return explodedNodes;
    }

    public Dataset<Row> explodeWays(final Dataset<Row> waysSet) {
        waysSet.cache();
        final Dataset<Row> explodedWays1 = waysSet.withColumn("tag", explode(col("tags")));
        final Dataset<Row> explodedWays2 = explodedWays1.withColumn("node", explode(col("nodes")));
        waysSet.unpersist();
        return explodedWays2;
    }
}
