package file_processing.udf;

import org.apache.spark.sql.Row;
import scala.Serializable;
import scala.collection.JavaConversions;
import scala.collection.Seq;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class MapConverter implements Serializable {

    public Map<String, String> convert(final Seq<Row> rowSeq) {
        final List<Row> rows = JavaConversions.seqAsJavaList(rowSeq);
        Map<String, String> map =  rows.stream().collect(Collectors
                .toMap(row -> row.getString(0),
                        row -> row.getString(1)));
        return  map;
    }

}
