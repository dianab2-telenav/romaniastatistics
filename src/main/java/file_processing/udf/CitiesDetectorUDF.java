package file_processing.udf;

import org.apache.spark.sql.Row;
import org.apache.spark.sql.api.java.UDF3;
import scala.collection.Seq;

import java.util.Map;


public class CitiesDetectorUDF implements UDF3<Seq<Row>, Integer, String, Boolean> {

    @Override public Boolean call(final Seq<Row> rowSeq, final Integer level, final String cityName) throws Exception {
        MapConverter mapConverter = new MapConverter();
        Map<String, String> tagsMap = mapConverter.convert(rowSeq);
        if ((tagsMap.containsKey("boundary") && tagsMap.get("boundary").equals("administrative"))
                && (tagsMap.containsKey("admin_level") && tagsMap.get("admin_level").equals(level.toString()))
            && (tagsMap.containsKey("name") &&tagsMap.get("name").contains(cityName)))
            return true;
        return false;
    }

}