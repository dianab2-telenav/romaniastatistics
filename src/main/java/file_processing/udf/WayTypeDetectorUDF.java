package file_processing.udf;

import org.apache.spark.sql.Row;
import org.apache.spark.sql.api.java.UDF2;
import scala.collection.Iterator;
import scala.collection.Seq;

import java.util.Map;


public class WayTypeDetectorUDF implements UDF2<Seq<Row>, String,  Boolean> {


    @Override public Boolean call(final Seq<Row> rowSeq, final String type) throws Exception {
        MapConverter mapConverter = new MapConverter();
        Map<String, String> map = mapConverter.convert(rowSeq);
        if (map.containsKey("highway") && map.get("highway").equals(type))
            return true;
        return false;
    }
}
