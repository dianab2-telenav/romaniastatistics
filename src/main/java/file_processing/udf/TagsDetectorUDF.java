package file_processing.udf;

import org.apache.spark.sql.Row;
import org.apache.spark.sql.api.java.UDF2;
import scala.collection.Seq;

import java.util.Map;


public class TagsDetectorUDF implements UDF2<Seq<Row>, String, Boolean> {

    @Override public Boolean call(final Seq<Row> rowSeq, final String tagKey) throws Exception {
        for(String type : TagValue.valueOf(tagKey).getValues())
        {
            MapConverter mapConverter = new MapConverter();
            Map<String, String> tagsMap = mapConverter.convert(rowSeq);
            if ((tagsMap.containsKey("building") && tagsMap.get("building").equals(type)) ||
                    tagsMap.containsKey("amenity") && tagsMap.get("amenity").equals(type))
                return true;
        }
        return false;
    }
}
