package file_processing.udf;

import java.io.Serializable;
import java.util.*;


public enum TagValue implements Serializable {
    CHURCH("Church", Arrays.asList("religious", "cathedral", "chapel", "church", "mosque", "temple", "synagogue", "shrine")),
    HIGHWAY("Highway", Arrays.asList("motorway", "trunk", "primary", "secondary", "tertiary", "unclassified", "residential", "service")),
    MEDICAL("Medical", Arrays.asList("clinic", "doctors", "hospital", "nursing_home", "dentist", "baby_hatch", "blood_donation")),
    EDUCATIONAL("Educational", Arrays.asList("school", "college", "kindergarten", "university", "music_school"));

    private String key;
    private List<String> values;

    TagValue(final String key, final List<String> values) {
        this.key = key;
        this.values = values;
    }

    public String getKey() {
        return key;
    }

    public List<String> getValues() {
        return values;
    }

    public TagValue getTagValuesForKey(String key) {
        for(TagValue tag: TagValue.values()) {
            if(tag.getKey().equals(key))
               return tag;
        }
        return null;
    }
}
