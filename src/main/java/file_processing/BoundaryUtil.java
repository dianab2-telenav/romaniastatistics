package file_processing;

import file_processing.models.*;
import org.apache.spark.api.java.JavaRDD;
import scala.Tuple2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class BoundaryUtil implements Serializable {

    public List<Tuple2<String, Double>> computeBounds(JavaRDD<Node> frontierPoints) {

        List<Tuple2<String, Double>> pointsList = new ArrayList<>();
        frontierPoints.cache();
        Double north = frontierPoints.mapToDouble(node -> node.getLatitude()).max();
        pointsList.add(new Tuple2<>("North", north));
        Double south = frontierPoints.mapToDouble(node -> node.getLatitude()).min();
        pointsList.add(new Tuple2<>("South", south));
        Double west = frontierPoints.mapToDouble(node -> node.getLongitude()).max();
        pointsList.add(new Tuple2<>("West", west));
        Double east = frontierPoints.mapToDouble(node -> node.getLongitude()).min();
        pointsList.add(new Tuple2<>("East", east));
        frontierPoints.unpersist();
        return pointsList;
    }

    public boolean isInside(List<Tuple2<String, Double>> boundsList, Node node) {
        if (node.getLatitude() < boundsList.get(1)._2() || node.getLatitude() > boundsList.get(0)._2())
            return false;
        if (node.getLongitude() < boundsList.get(3)._2() || node.getLongitude() > boundsList.get(2)._2())
            return false;
        return true;
    }
}
