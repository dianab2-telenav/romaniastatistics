package file_processing.models;

import java.io.Serializable;


public class Tag implements Serializable {
    private String key, value;

    public Tag() { }

    public Tag(final String key, final String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(final String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
