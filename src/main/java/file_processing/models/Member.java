package file_processing.models;

import java.io.Serializable;


public class Member implements Serializable {

    private long id;
    private String role, type;

    public Member() {}

    public Member(final long id, final String role, final String type) {

        this.id = id;
        this.role = role;
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(final String role) {
        this.role = role;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }
}
