package file_processing.models;

import java.io.Serializable;


public class IndexedNode implements Serializable, Comparable {

    private Integer index;
    private Node node;

    public Integer getIndex() {
        return index;
    }

    public Node getNode() {
        return node;
    }

    public IndexedNode(final Integer index, final Node node) {

        this.index = index;
        this.node = node;
    }

    @Override public int compareTo(final Object o) {

        IndexedNode indexedNode = (IndexedNode)o;
        return this.getIndex()-indexedNode.getIndex();
    }
}
