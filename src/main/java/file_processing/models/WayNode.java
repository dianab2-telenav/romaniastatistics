package file_processing.models;

import java.io.Serializable;


public class WayNode implements Serializable {
    private int index;
    private long nodeId;

    public WayNode(final int index, final long nodeId) {
        this.index = index;
        this.nodeId = nodeId;
    }

    public WayNode() { }

    public int getIndex() {
        return index;
    }

    public void setIndex(final int index) {
        this.index = index;
    }

    public long getNodeId() {
        return nodeId;
    }

    public void setNodeId(final long nodeId) {
        this.nodeId = nodeId;
    }

}
