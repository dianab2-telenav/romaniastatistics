package file_processing.models;

import scala.Tuple2;

import java.io.Serializable;


public class Node implements Serializable {

    private long id;
    private int version;
    private long timestamp;
    private long changeset;
    private int uid;
    private Tag[] tags;
    private double latitude, longitude;

    public Node() { }

    public Node(final long id, final int version, final long timestamp, final long changeset, final int uid,
            final String user_sid, final Tag[] tags, final double latitude, final double longitude) {

        this.id = id;
        this.version = version;
        this.timestamp = timestamp;
        this.changeset = changeset;
        this.uid = uid;
        //        this.user_sid = user_sid;
        this.tags = tags;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public long getId() {

        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final long timestamp) {
        this.timestamp = timestamp;
    }

    public long getChangeset() {
        return changeset;
    }

    public void setChangeset(final long changeset) {
        this.changeset = changeset;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(final int uid) {
        this.uid = uid;
    }

    public Tag[] getTags() {
        return tags;
    }

    public void setTags(final Tag[] tags) {
        this.tags = tags;
    }

    public double getDistance(Node x) {
        double distance = Math.sqrt(
                (this.getLatitude() - x.getLatitude()) * 111 * (this.getLatitude() - x.getLatitude()) * 111
                        + (this.getLongitude() - x.getLongitude()) * 88 * (this.getLongitude() - x.getLongitude())
                        * 88);
        System.out.println(distance);
        return distance;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(final double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(final double longitude) {
        this.longitude = longitude;
    }
}
