package file_processing.models;

import java.io.Serializable;


public class Relation implements Serializable {
    private long id;
    private int version;
    private long timestamp;
    private long changeset;
    private int uid;
    private String user_sid;
    private Tag[] tags;
    private Member[] members;

    public Relation(final long id, final int version, final long timestamp, final long changeset, final int uid,
            final String user_sid, final Tag[] tags, final Member[] members) {
        this.id = id;
        this.version = version;
        this.timestamp = timestamp;
        this.changeset = changeset;
        this.uid = uid;
        this.user_sid = user_sid;
        this.tags = tags;
        this.members = members;
    }

    public Relation() { }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final long timestamp) {
        this.timestamp = timestamp;
    }

    public long getChangeset() {
        return changeset;
    }

    public void setChangeset(final long changeset) {
        this.changeset = changeset;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(final int uid) {
        this.uid = uid;
    }

    public String getUser_sid() {
        return user_sid;
    }

    public void setUser_sid(final String user_sid) {
        this.user_sid = user_sid;
    }

    public Tag[] getTags() {
        return tags;
    }

    public void setTags(final Tag[] tags) {
        this.tags = tags;
    }

    public Member[] getMembers() {
        return members;
    }

    public void setMembers(final Member[] members) {
        this.members = members;
    }
}
