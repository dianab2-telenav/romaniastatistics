package file_processing;

import org.apache.spark.sql.SparkSession;

import java.io.Serializable;


public class Main implements Serializable {

    public static void main(String args[]) {
        SparkSession spark = SparkSession.builder()
                .master("local[*]")
                .config("spark.sql.parquet.binaryAsString", "true")
                .appName("Simple Application")
                .getOrCreate();
        spark.sparkContext().setLogLevel("ERROR");

        ApplicationStarter appStarter = new ApplicationStarter(spark);
//        appStarter.startChurchCounter(spark);
//        appStarter.startMedicalCounter(spark);
//        appStarter.startSchoolCounter(spark);
        appStarter.startCityCounter(spark);
    }
}
