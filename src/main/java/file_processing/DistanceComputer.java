package file_processing;

import file_processing.models.IndexedNode;
import file_processing.models.Node;
import org.apache.spark.api.java.JavaPairRDD;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class DistanceComputer implements Serializable {

    private Double computePartial(List<IndexedNode> wayList) {
        wayList.sort((x, y) -> x.getIndex() - y.getIndex());
        Double sum = 0.0;
        for (int i = 0; i < wayList.size() - 1; i++) {
            System.out.println(wayList.get(i).getIndex() + " + " + wayList.get(i + 1).getIndex());
            sum += this.distanceBetweenTwoNodes(wayList.get(i + 1).getNode(), wayList.get(i).getNode());
        }
        return sum;
    }

    private Double distanceBetweenTwoNodes(Node x, Node y) {
        double earthRadius = 6371.01; //Kilometers
        return earthRadius * Math.acos(
                Math.sin(y.getLatitude()) * Math.sin(x.getLatitude())
                        + Math.cos(y.getLatitude()) * Math.cos(x.getLatitude())
                        * Math.cos(y.getLongitude() - x.getLongitude()));
    }

    private List<IndexedNode> buildList(Iterable<IndexedNode> wayIterable) {
        Iterator<IndexedNode> wayIterator = wayIterable.iterator();
        List<IndexedNode> wayList = new ArrayList<>();
        while (wayIterator.hasNext()){
            wayList.add(wayIterator.next());
        }
        return wayList;
    }

    public Double compute(final JavaPairRDD<Long, IndexedNode> rowsData) {

        return rowsData.groupByKey().mapToDouble(pair ->  {
            List<IndexedNode> wayList = buildList(pair._2());
            Double sum = 0.0;
            sum += computePartial(wayList);
            return sum;
        }).sum();

    }
}
