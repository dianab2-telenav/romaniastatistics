package file_processing;

import file_processing.models.IndexedNode;
import file_processing.models.Node;
import file_processing.models.Way;
import file_processing.models.WayNode;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import scala.Tuple2;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class DataJoiner implements Serializable {

    public JavaPairRDD<Long, IndexedNode> join(JavaRDD<Way> waysRDD, JavaRDD<Node> nodesRDD) {

        JavaPairRDD<Long, Node> idAndNode = nodesRDD.mapToPair(node -> new Tuple2<>(node.getId(), node));
        JavaPairRDD<Long, Tuple2<Long, Integer>> wayAndNodeIdAssoc = expandWayNodes(waysRDD);
        return wayAndNodeIdAssoc.join(idAndNode)
                .mapToPair(y -> new Tuple2<>(getWayId(y), new IndexedNode(getNodeIndex(y), getNode(y))));
    }

    private JavaPairRDD<Long, Tuple2<Long, Integer>> expandWayNodes(JavaRDD<Way> waysRDD) {
        return waysRDD.mapToPair(way -> new Tuple2<>(way, way.getNodes())).flatMapToPair(pair -> {
            List<WayNode> list = Arrays.asList(pair._2());
            return list.stream().map(node -> new Tuple2<>(pair._1(), node)).collect(Collectors.toList()).iterator();
        }).mapToPair(pair -> new Tuple2<>(pair._2().getNodeId(),
                new Tuple2<>(pair._1().getId(), pair._2().getIndex())));//node id and way id
    }

    private Long getWayId(Tuple2<Long, Tuple2<Tuple2<Long, Integer>, Node>> carnatMare) {
        return carnatMare._2()._1()._1();
    }

    private Integer getNodeIndex(Tuple2<Long, Tuple2<Tuple2<Long, Integer>, Node>> carnatMare) {
        return carnatMare._2()._1()._2();
    }

    private Node getNode(Tuple2<Long, Tuple2<Tuple2<Long, Integer>, Node>> carnatMare) {
        return carnatMare._2()._2();
    }

    public JavaRDD<Node> mapToNodeList(JavaPairRDD<Long, IndexedNode> indexedNodes) {
        return indexedNodes.map(pair -> pair._2().getNode());
    }

}
