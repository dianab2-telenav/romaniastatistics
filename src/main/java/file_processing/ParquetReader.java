package file_processing;

import file_processing.models.Node;
import file_processing.models.Relation;
import file_processing.models.Way;
import org.apache.spark.sql.*;

import java.io.Serializable;


public class ParquetReader implements Serializable {

    public Dataset<Row> read(final String sourcePath, final SparkSession sparkSession) {
        final Dataset<Row> rowData = sparkSession.read().parquet(sourcePath);
        return rowData;
    }

    public Dataset<Node> readNodes(final String sourcePath, final SparkSession sparkSession) {
        final Dataset<Node> nodeData = sparkSession.read()
                .parquet(sourcePath)
                .as(Encoders.bean(Node.class));
        return nodeData;
    }
    public Dataset<Way> readWays(final String sourcePath, final SparkSession sparkSession) {
        final Dataset<Way> wayData = sparkSession.read()
                .parquet(sourcePath)
                .as(Encoders.bean(Way.class));
        return wayData;
    }
    public Dataset<Relation> readRelations(final String sourcePath, final SparkSession sparkSession) {
        final Dataset<Relation> relationData = sparkSession.read()
                .parquet(sourcePath)
                .as(Encoders.bean(Relation.class));
        return relationData;
    }

}
